package class01;

/*
https://leetcode-cn.com/problems/volume-of-histogram-lcci/
给定一个直方图(也称柱状图)，假设有人从上面源源不断地倒水，最后直方图能存多少水量?直方图的宽度为 1。
上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的直方图，在这种情况下，可以接 6 个单位的水（蓝色部分表示水）。感谢 Marcos 贡献此图。

示例:

输入: [0,1,0,2,1,0,1,3,2,1,2,1]
输出: 6

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/volume-of-histogram-lcci
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
hint:
请从每个长方形的角度来考虑。每个长方形上面都有水。每个长方形上面会有多少水？
每个长方形的顶部都有水，水的高度应与左侧最高长方形和右侧最高长方形的较小值相匹配，也就是说，water_on_top[i] = min(tallest_ bar(0->i), tallest_bar(i, n))。
维护两个列表，分别记录对每个直方图来说，左边的最大值，和右边的最大值；单个直方图上的水量就是 min(leftMax, rightMax) - height
 */
public class Water {
    public static int water1(int[] arr) {
        int len = arr.length;
        int res = 0;
        if (len < 3) {
            return 0;
        }
        int[] leftMax = new int[len];
        int[] rightMax = new int[len];
        int left = arr[0];
        for (int i=0 ; i< len; i++) {
            if (left < arr[i]) {
                left = arr[i];
            }
            leftMax[i] = left;
        }
        int right = arr[len-1];
        for (int i = len-1; i>=0; i --) {
            if (right < arr[i]) {
                right = arr[i];
            }
            rightMax[i] = right;
        }

        for (int i = 0; i < len; i++) {
            int less = Math.min(leftMax[i], rightMax[i]);
            if (arr[i] < less) {
                res += less - arr[i];
            }
        }

        return res;
    }

    public static void main(String[] args) {
        int[] data1 = new int[12];
        data1 [0] = 0;
        data1 [1] = 1;
        data1 [2] = 0;
        data1 [3] = 2;
        data1 [4] = 1;
        data1 [5] = 0;
        data1 [6] = 1;
        data1 [7] = 3;
        data1 [8] = 2;
        data1 [9] = 1;
        data1 [10] = 2;
        data1 [11] = 1;
        System.out.println(water1(data1));
    }
}
