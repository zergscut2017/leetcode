package class02;

/*
https://leetcode-cn.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/
根据一棵树的后序遍历构造二叉树。

注意:
你可以假设树中没有重复的元素。

例如，给出

后序遍历 postorder = [9,15,7,20,3]
返回如下的二叉树：

    3
   / \
  9  20
    /  \
   15   7

 */


import java.util.ArrayList;

public class PostArrBST {

    public static TreeNode posArrayToBST1(int[] postArr) {
        return build1(postArr, 0, postArr.length-1);
    }

    public static TreeNode build1(int[] postArr, int L, int R) {
        // O(N*N)
        if (L > R) {
            return null;
        }
        TreeNode root = new TreeNode(postArr[R]);

        if (L == R) {
            return root;
        }

        int M = L - 1;
        for (int i = L; i < R; i++) {
            if (postArr[i] < postArr[R]) {
                M = i;
            }
        }
        root.left = build1(postArr, L, M);
        root.right = build1(postArr, M+1, R-1);
        return  root;
    }

    public static TreeNode posArrayToBST2(int[] posArr) {
        return build2(posArr, 0, posArr.length - 1);
    }

    public static TreeNode build2(int[] postArr, int L, int R) {
        // O(Nlog(N))
        if (L > R) {
            return null;
        }
        TreeNode root = new TreeNode(postArr[R]);
        if (L == R) {
            return root;
        }

        int M = L - 1;
        int left = L;
        int right = R - 1;
        while (left <= right) {
            int mid = left + (right - left) >> 1;// >>1 等价于/2，但是跟高效
            //为什么不能用 M = (L + R) /2 ? 因为L+R 都是int，可能会越界哦
            if (postArr[mid] < postArr[R]){
                M = mid;
                left = mid + 1;
            } else{
                right = mid - 1;
            }
        }
        root.left = build2(postArr, L, M);
        root.right = build2(postArr, M + 1, R - 1);

        return root;
    }


    public static int[] getBstPosArray(TreeNode root) {
        ArrayList<Integer> posList = new ArrayList<>();
        postTravel(root, posList);
        int[] ans = new int[posList.size()];
        for (int i = 0; i< ans.length; i++) {
            ans[i] = posList.get(i);
        }
        return ans;
    }

    public static void postTravel(TreeNode root, ArrayList<Integer> arr) {
        if (root != null) {
            postTravel(root.left, arr);
            postTravel(root.right, arr);
            arr.add(root.val);
        }
    }

    public static int random(int min, int max) {
        return min + (int) (Math.random() * (max - min + 1));
    }

    public static TreeNode createTree(int min, int max, int level, int N) {
        if ( min > max || level > N) {
            return null;
        }
        TreeNode root = new TreeNode(random(min, max));
        root.left = createTree(min, root.val-1, level + 1, N);
        root.right = createTree(root.val+1, max, level + 1, N);
        return root;
    }

    public static TreeNode generateBST(int min, int max, int N) {
        if (min > max) {
            return null;
        }
        return createTree(min, max, 1 , N);
    }

    public static void printInOrder(TreeNode head, int height, String to, int len) {
        if (head == null) {
            return;
        }
        printInOrder(head.right, height + 1, "v", len);
        String val = to + head.val + to;
        int lenM = val.length();
        int lenL = (len - lenM) / 2;
        int lenR = len - lenM - lenL;
        val = getSpace(lenL) + val + getSpace(lenR);
        System.out.println(getSpace(height * len) + val);
        printInOrder(head.left, height + 1, "^", len);
    }

    public static void printTree(TreeNode head) {
        System.out.println("Binary Tree:");
        printInOrder(head, 0, "H", 17);
        System.out.println();
    }

    public static String getSpace(int num) {
        String space = " ";
        StringBuffer buf = new StringBuffer("");
        for (int i = 0; i < num; i++) {
            buf.append(space);
        }
        return buf.toString();
    }

    public static boolean isSameValueStructure(TreeNode head1, TreeNode head2) {
        if (head1 == null && head2 != null) {
            return false;
        }
        if (head1 != null && head2 == null) {
            return false;
        }
        if (head1 == null && head2 == null) {
            return true;
        }
        return head1.val == head2.val && isSameValueStructure(head1.left, head2.left)
                && isSameValueStructure(head1.right, head2.right);
    }

    public static void main(String[] args) {
        int min = 0;
        int max = 12;
        int level = 4;
        TreeNode root = generateBST(min, max, level);
        int[] posArr = getBstPosArray(root);
        printTree(root);
        printTree(posArrayToBST1(posArr));
        printTree(posArrayToBST2(posArr));
        System.out.println(isSameValueStructure(root, posArrayToBST1(posArr)));
        System.out.println(isSameValueStructure(root, posArrayToBST2(posArr)));

        int testTimes = 500000;
        System.out.println("test begin, time time : " + testTimes);
        for (int i = 0; i < testTimes; i++) {
            root = generateBST(min, max, level);
            posArr = getBstPosArray(root);
            if (!isSameValueStructure(root, posArrayToBST1(posArr)) || !isSameValueStructure(root, posArrayToBST2(posArr))) {
                System.out.println("Oops!");
            }
        }
        System.out.println("test finish");
    }
}
